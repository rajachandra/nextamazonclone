import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from "react-responsive-carousel";

function Banner() {
  return (
    <div className="relative pt-24">
      <div className="absolute w-full h-32 bg-gradient-to-t from-gray-100 to-transparent bottom-0 z-20" />
      <Carousel
        autoPlay
        infiniteLoop
        showIndicators={false}
        showStatus={false}
        showThumbs={false}
        interval={1000}
      >
        <div>
          <img
            src="https://links.papareact.com/gi1"
            alt="slide1"
            loading="lazy"
          />
        </div>
        <div>
          <img
            src="https://links.papareact.com/6ff"
            alt="slide1"
            loading="lazy"
          />
        </div>
        <div>
          <img
            src="https://links.papareact.com/7ma"
            alt="slide1"
            loading="lazy"
          />
        </div>
      </Carousel>
    </div>
  );
}

export default Banner;
